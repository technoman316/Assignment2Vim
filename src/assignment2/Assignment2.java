package assignment2;
import java.util.Scanner;
public class Assignment2 {
	/**
	 * A program to calculate the sum, average, max, min, and, median of 5
	 * numbers
	 * 
	 * @author cjp2790
	 * Date: 9/17/14
	 * Algorithm:
	 * 1. Take in 5 numbers
	 * 2. Calculate the 5 operations
	 * 3. Display the answer to the screen
	 */
	
	public static void main(String[] args) {
		double n1, n2, n3, n4, n5;
		// Take in 5 numbers
		System.out.println("Enter 5 numbers in order of least to greatest with spaces and without repeating numbers:");
		Scanner numbers = new Scanner(System.in);
		n1 = numbers.nextInt();
		n2 = numbers.nextInt();
		n3 = numbers.nextInt();
		n4 = numbers.nextInt();
		n5 = numbers.nextInt();
		double average;
		average = ((n1 + n2 + n3 + n4 + n5) / 5);
		
		if (n1 < n2 && n2 < n3 && n3 < n4 && n4 < n5){
			// Calculate the sum
			System.out.println("The sum is " + (n1 + n2 + n3 + n4 + n5));
			
			// Calculate the average
			System.out.println("The average is " + average);
			
			// Calculate the max
			System.out.println("The max is " + n5);
			
			// Calculate the min
			System.out.println("The min is " + n1);
			
			//Calculate the median
			System.out.println("The median is " + n3);
		}
		else{
			System.out.println("I said put them in order from least to greatest");
			System.out.println("Run the command again");
		}
	}
}
